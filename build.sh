SRC=./src/main/
TESTS=./src/tests

if [ "$1" = "build" ]
then
	javac $SRC/*.java
	exit 0
elif [ "$1" = "clean" ]
then
	rm $SRC/*.class
	exit 0
elif [ "$1" = "test" ]
then
	javac $TESTS/*.java
	exit 0
elif [ "$1" = "clean-tests" ]
then
	rm $TESTS/*.class
	exit 0
else
	echo "Unknown command try again"
	exit 1
fi


