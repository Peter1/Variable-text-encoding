package main;

public class main {
    public static void main(String[] args) {
        HuffmanCodeBook dictionary = ProvidedHuffmanCodeBook.getEbookHuffmanCodebook();
        HuffmanCodeTree tree = new HuffmanCodeTree(dictionary);

        if (args.length != 2) {
            System.out.println("Usage: [arg] [file]");
            System.out.println(args.length);
            return;
        }

        String text;
        switch(args[0]) {
            case "-e":
               text = FileIOAssistance.readFile(args[1]);
                BinarySequence encodedText = dictionary.encode(text);
                System.out.println(args[1].replace(".txt", ".enc"));
                encodedText.writeToFile(args[1].replace(".txt", ".enc"));
                break;

            case "-d":
                BinarySequence bits = BinarySequence.readFromFile(args[1]);
                String unencodedText = tree.decode(bits);
                FileIOAssistance.writeFile(args[1].replace(".txt", ".enc"), unencodedText);
                break;
            
            default:
                System.out.println("-d to decode -e to encode");
        }
    }
}
