package main;

/** Linked list (Bucket) that is used to handle collisions. */
public class Bucket {
    /** The char (key) in bucket. */
    private Character key;
    /** The BinarySequence (value) in bucket. */
    private BinarySequence value;
    /** The next bucket in list. */
    private Bucket next;

    public Bucket getNext() {
        return this.next;
    }

    public void setNext(Bucket bucket) {
        this.next = bucket;
    }

    public BinarySequence getValue() {
        return this.value;
    }

    public void setValue(BinarySequence seq) {
        this.value = seq;
    }

    public Character getKey() {
        return this.key;
    }

    public void setKey(char c) {
        this.key = c;
    }

    /**
     * Recursive method that adds a bucket to the end of a list.
     *
     * @param bucket the bucket to add
     */
    public void put(Bucket bucket) {
        if (this.value == null && this.key == null) {
            this.key = bucket.getKey();
            this.value = bucket.getValue();
        } else if (this.key == bucket.getKey()) {
            this.value = bucket.getValue();
        } else if (this.next == null) {
            this.next = bucket;
        } else {
            this.next.put(bucket);
        }
    }

    /**
     * Same as above, but takes a char and a seq.
     *
     * @param c the key to add
     * @param seq the associated value
     */
    public void put(char c, BinarySequence seq) {
        if (this.value == null && this.key == null) {
            this.key = c;
            this.value = seq;
        } else if (this.key == c) {
            this.value = seq;
        } else if (this.next == null) {
            this.next = new Bucket(c, seq);
        } else {
            this.next.put(c, seq);
        }
    }

    /**
     * Recursive method that gets the value associated with a key.
     *
     * @param key the key
     * @return the value associated with the given key
     */
    public BinarySequence get(char key) {
        if (this == null || (this.value == null && this.key == null)) {
            return null;
        } else if (this.key == key) {
            return this.value;
        } else if (this.next == null) {
            return null;
        } else {
            return this.next.get(key);
        }
    }

    /**
     * Bucket class constructor. Sets the key and value.
     *
     * @param key the key
     * @param value the value to associate with the key
     */
    public Bucket(char key, BinarySequence value) {
        this.key = key;
        this.value = value;
    }

    public Bucket() { }
}
