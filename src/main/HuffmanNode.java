package main;

/**
 * @Author Peter Zimmermann
 *
 * Node representation for the HuffmanTree class. It represents
 * a single node holding values for the 0, 1 and value of each node.
 */
public class HuffmanNode {
    /** The "zero" next node. */
    private HuffmanNode zero;
    /** The "one" next node. */
    private HuffmanNode one;
    /** the character value being stored in the node. */
    private Character   value;

    public HuffmanNode getZero() {
        return this.zero;
    }

    public void setZero(HuffmanNode zero) {
        this.zero = zero;
    }

    public HuffmanNode getOne() {
        return this.one;
    }

    public void setOne(HuffmanNode one) {
        this.one = one;
    }

    public Character getData() {
        return this.value;
    }

    public void setData(char c) {
        this.value = c;
    }

    public boolean isLeaf() {
        return this.zero == null && this.one == null && this.value != null;
    }

    /**
     * Determines if the current node is valid. In order to be valid
     * the node either needs to be a leaf or it needs to have both a
     * one and a zero and not store a value
     *
     * @return boolean whether or not the current node is valid
     */
    public boolean isValidNode() {
        return this.isLeaf()
        ^ // Use XOR here
        // Check if it is a valid inner node
        (this.value == null && this.zero != null && this.one != null);
    }

    /**
     * verifies that the binary tree is valid starting from this node.
     *
     * @return boolean whether or not the tree from this node is valid
     */
    public boolean isValidTree() {
        if (this.zero == null && this.one == null) {
            return this.value != null;
        }

        // Recursive call is here
        return this.isValidNode() && this.zero.isValidTree() && this.one.isValidTree();
    }

    /**
     * constructs a parent node with a one and zero node but no value.
     *
     * @param zero the zero node from this node
     * @param one the one node from this node
     */
    public HuffmanNode(HuffmanNode zero, HuffmanNode one) {
        this.zero = zero;
        this.one = one;
    }

    /**
     * constructs a leaf node with a value but no one or zero.
     *
     * @param value the character stored in this node
     */
    public HuffmanNode(char value) {
        this.value = value;
        this.zero = null;
        this.one = null;
    }
}
