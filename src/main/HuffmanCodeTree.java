package main;

/**
 * @Author Peter Zimmermann
 *
 * Binary tree for the huffman encoding.
 */
public class HuffmanCodeTree {
    /** The root node of the tree. */
    private HuffmanNode root;

    public boolean isValid() {
        return this.root.isValidTree();
    }

    /**
     * Adds a character in the appropriate location at the binary tree.
     * Does this by iterating over the binary sequence and going to the
     * corresponding node for the value in the sequence. If the node does
     * not exist is creates the node with two null children nodes. This
     * might be able to be optimized because once the node is null it
     * will be null for the rest of the sequence. Once the end of the
     * sequence is reached, it sets the data at the current node to the
     * desired character. Note: this will not always create a valid
     * binary tree.
     *
     * @param seq the binary sequence to follow
     * @param c the character to add at the end of the sequence
     */
    public void put(BinarySequence seq, char c) {
        HuffmanNode currentNode = root;
        for (boolean b : seq) {
            // If b is true, create a one node if false a zero node
            if (b) {
                // If the one node does not exist create a new one node
                if (currentNode.getOne() == null) {
                    currentNode.setOne(new HuffmanNode(null, null));
                }
                currentNode = currentNode.getOne();
            } else {
                // If the zero node does not exist create a new one
                if (currentNode.getZero() == null) {
                    currentNode.setZero(new HuffmanNode(null, null));
                }
                currentNode = currentNode.getZero();
            }
        }
        currentNode.setData(c);
    }

    /**
     * Decodes a given binary sequence s by iterating over it and getting
     * the corresponding node. Once a character has been reached it is
     * added the the string and it resets at the root node. This might
     * be able to be optimized by chacheing common characters (ex spaces)
     *
     * @param s the sequence to decode
     * @return value of the decoded sequence
     */
    public String decode(BinarySequence s) {
        HuffmanNode node = this.root;
        StringBuilder decodedSequence = new StringBuilder();
        for (boolean b : s) {
            if (b) {
                node = node.getOne();
            } else {
                node = node.getZero();
            }

            if (node.isLeaf()) {
                decodedSequence.append(node.getData());
                node = this.root;
            }
        }
        return decodedSequence.toString();
    }

    public HuffmanCodeTree(HuffmanNode root) {
        this.root = root;
    }

    /**
     * Constructs a HuffmanCodeTree given a codebook. Iterates over the
     * codebook and creates a new node for each character.
     *
     * @param codebook the codebook used to construct the treee
     */
    public HuffmanCodeTree(HuffmanCodeBook codebook) {
        this.root = new HuffmanNode(null, null);
        for (char c : codebook) {
            this.put(codebook.getSequence(c), c);
        }
    }
}
