package main;

import java.util.Iterator;
import java.lang.Character;

/**
 * @Author Peter Zimmermann
 *
 * Hashtable Representation of a codebook. It stores the characters
 * and the corresponding binary sequences using a dictionary.
 */
public class HuffmanCodeBook implements Iterable<Character> {
    /**
     * Vector (array list) of characters stored in the hash table. Used
     * for iteration in the HuffmanCodeTree class.
     */
    private Vector<Character> chars;

    /**
     * Array of buckets. Primary variable that will be used in the
     * hashtable.
     */
    private Bucket[] bucketArray;

    /**
     * Current buckets capacity of table. Only includes the first bucket
     * in the array and not every single bucket.
     */
    private int numBuckets;

    /**
     * Total amount of buckets in table being used. used along with
     * numBuckets to calculate load.
     */
    private int size;

    private int getIndex(Character c) {
        return c.hashCode() % this.numBuckets;
    }

    /** Resizes the BucketArray when the load factor is too high. */
    private void resize() {
        int newNumBuckets = numBuckets * 2;
        Bucket[] newBucketArray = new Bucket[newNumBuckets];

        int newIndex;
        char charToAdd;
        for (Bucket b : this.bucketArray) {
            while (b != null) {
                charToAdd = b.getKey();
                newIndex = ((int) charToAdd) % newNumBuckets;
                if (newBucketArray[newIndex] == null) {
                    newBucketArray[newIndex] = b;
                } else {
                    newBucketArray[newIndex].put(b);
                }
                b = b.getNext();
            }
        }
        numBuckets = newNumBuckets;
        bucketArray = newBucketArray;
    }

    /**
     * Adds a key value pair to the hashtable.
     *
     * @param c the character to add (key)
     * @param seq the BinarySequence to add (value)
     */
    public void addSequence(char c, BinarySequence seq) {
        this.chars.append(c);
        int index = this.getIndex(c);
        Bucket bucket = this.bucketArray[index];

        if (bucket == null) {
            bucket = new Bucket(c, seq);
            this.bucketArray[index] = bucket;
        } else {
            bucket.put(c, seq);
        }
        ++size;

        double load = (double) size / numBuckets;
        if (load > 0.65) {
            this.resize();
        }
    }

    /**
     * Retrievs a BinarySequence given the associated character.
     *
     * @param c the character/key
     * @return the associated BinarySequence
     */
    public BinarySequence getSequence(char c) {
        int index = this.getIndex(c);
        Bucket bucket = this.bucketArray[index];

        if (bucket == null) {
            return null;
        }
        return bucket.get(c);
    }

    /**
     * Encodes the binary sequence by making sure each character has an
     * encoding. If they do not have an encoding it throws an exception.
     * If they do all have an encoding, it iterates over the string and
     * encodes each character, appending each encoding to the BinarySequence
     * That will be returned.
     *
     * @param inputString the string that will be encoded
     * @return the binary sequence of the encoded string
     */
    public BinarySequence encode(String inputString) {
        if (!this.containsAll(inputString)) {
            throw new RuntimeException("Not all chars have an encoding");
        }

        BinarySequence seq = new BinarySequence();
        for (char c : inputString.toCharArray()) {
            seq.append(this.getSequence(c));
        }

        return seq;
    }

    public boolean contains(char c) {
        // returns true if the sequence is in the table otherwise false
        return this.getSequence(c) == null ? false : true;
    }

    /**
     * Checks to see that each character in a given string is contained
     * in the input string. Does this by iterating over all the characters
     * in the string and returning false if there is one not encoded.
     *
     * @param s the string that will be checked
     * @return whether every character is encoded or not
     */
    public boolean containsAll(String s) {
        for (char c : s.toCharArray()) {
            if (this.contains(c) == false) {
                return false;
            }
        }
        return true;
    }

    public Iterator<Character> iterator() {
        return this.chars.iterator();
    }

    /**
     * A constructor for the HuffmanCodeBook. Takes no arguments. Creates
     * a new vector for the characters, a new bucket array length 64
     * and initalizes the numBuckets and size to their appropriate values.
     */
    public HuffmanCodeBook() {
        chars = new Vector<Character>();
        bucketArray = new Bucket[64];
        this.numBuckets = 64;
        this.size = 0;
    }
}
