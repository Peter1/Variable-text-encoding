package main;

import java.util.Iterator;

/**
 * Vector represents the list of characters so that
 * I can iterate over this class later.
 *
 * @param <T> type contained in the vector
 */
public class Vector<T> implements Iterable<T> {
    /** Array of characters. */
    private T[] elements;
    /** length of the array. */
    private int length;
    /** current index of last element. */
    private int currentIndex;

    /**
     * Adds a character to the end of the list.
     *
     * @param element the character to add
     */
    public void append(T element) {
        if (this.length <= this.currentIndex) {
            T[] tmp = (T[]) new Object[this.length * 2];
            System.arraycopy(this.elements, 0, tmp, 0, this.length);
            this.length *= 2;
            this.elements = tmp;
        }
        this.elements[this.currentIndex] = element;
        ++this.currentIndex;
    }

    public int getSize() {
        return this.currentIndex;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Returns the iterator used to iterate over vector. Implements
     * iterator interface.
     *
     * @return a new iterator
     */
    @Override
    public Iterator<T> iterator() {
        /* The iterator to return **/
        Iterator<T> it = new Iterator<T>() {
            /** Index of the iterator */
            private int iteratorIndex = 0;

            @Override
            public boolean hasNext() {
                return this.iteratorIndex < length && elements[this.iteratorIndex] != null;
            }

            @Override
            public T next() {
                return elements[this.iteratorIndex++];
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
        return it;
    }

    /**
     * Constructor for this class. 67 because it is a prime.
     */
    public Vector() {
        this.elements = (T[]) new Object[67];
        this.length = 32;
        this.currentIndex = 0;
    }
}
