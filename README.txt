In order to run this code run the 'build.sh' file. Then, in the src
directory, run the command java main/main.java with the appropriate
arguments. If you wish to encrypt a provided ebook, run main/main.java
-e [path-to-ebook]. To decrypt an ebook, run main/main.java -d
[path-to-encrypted-ebook].
